import Vue from 'vue'
import VueRouter from 'vue-router'
import BubblingSort from '../views/BubblingSort.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/bubbling',
    name: 'BubblingSort',
    component: BubblingSort
  },
  {
    path: '/bubbling-up',
    name: 'BubblingSortUp',
    component: () => import('../views/BubblingSortUp.vue')
  },
  {
    path: '/',
    name: 'Home',
    redirect: '/bubbling'
  },
  {
    path: '/select',
    name: 'SelectSort',
    component: () => import('../views/SelectSort.vue')
  },
  {
    path: '/select-up',
    name: 'SelectSortUp',
    component: () => import('../views/SelectSortUp.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
