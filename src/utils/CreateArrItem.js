/*
 * @Author: zws
 * @Date: 2021-06-02 10:52:34
 * @LastEditTime: 2021-06-02 10:53:04
 * @LastEditors: zws
 * @Description:
 * @FilePath: \array_sort\src\utils\CreateArrItem.js
 */

export class CreateArrItem {
    constructor(value, index) {
        this.value = value;
        this.index = index;
        this.select = false;
    }
}